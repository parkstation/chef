cookbook_file "/etc/nginx/sites-enabled/parkshop" do
  owner "root"
  group "root"
  mode "0655"
  notifies :restart, resources(:service => "nginx"), :delayed
end

cookbook_file "/etc/nginx/sites-enabled/parkshop_admin" do
  owner "root"
  group "root"
  mode "0655"
  notifies :restart, resources(:service => "nginx"), :delayed
end
