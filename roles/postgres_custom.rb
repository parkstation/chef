name 'postgres_custom'
description 'postgres role'

run_list(
  "recipe[postgresql::server]",
  "recipe[postgresql::server_dev]",
  "recipe[postgresql::client]",
)

default_attributes(
  'postgresql' => {
    'version' => '9.3',
    'users' => [
      {
        'username' => 'deployer',
        'encrypted_password' => 'md5ca84a5d95efbc17d4c8c604152600291',
        'superuser' => true,
        'createdb' => true,
        'login' => true
      },
      {
        'username' => 'test',
        'encrypted_password' => 'md5fa437b61f8c41d35aaf124a8be62f409',
        'superuser' => true,
        'createdb' => true,
        'login' => true
      }
    ],
    'pg_hba' => [
      {'type' => 'local', 'db' => 'all', 'user' => 'postgres', 'addr' => '', 'method' => 'peer' },
      {'type' => 'local', 'db' => 'all', 'user' => 'all', 'addr' => '', 'method' => 'peer' },
      {'type' => 'host', 'db' => 'all', 'user' => 'all', 'addr' => '127.0.0.1/32', 'method' => 'md5'},
      {'type' => 'host', 'db' => 'all', 'user' => 'all', 'addr' => '::1/128', 'method' => 'md5'},
      {'type' => 'host', 'db'  => 'all', 'user' => 'all', 'addr' => 'all', 'method' => 'password'}
    ],
    'conf' => {
      'listen_addresses' => '*'
    },
    'databases' => [
      {
        'name' => 'parkshop_production',
        'owner' => 'deployer',
        'template' => 'template0',
        'encoding' => 'UTF-8',
        'locale' => 'en_US.UTF-8'
      }
    ]
  }
)
