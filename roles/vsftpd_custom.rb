name 'vsftpd_custom'
description 'custom vsftpd role'

run_list(
  'recipe[vsftpd]'
)

default_attributes(
  'vsftpd' => {
    'config' => {
      'anonymous_enable' => 'NO',
      'local_enable' => 'YES',
      'write_enable' => 'YES',
      'local_umask' => '022',
      'dirmessage_enable' => 'YES',
      'xferlog_enable' => 'YES',
      'connect_from_port_20' => 'YES',
      'xferlog_std_format' => 'YES',
      'listen' => 'YES',
      'pam_service_name' => 'vsftpd',
      'userlist_enable' => 'YES',
      'userlist_deny' => 'NO',
      'tcp_wrappers' => 'YES'
    }
  }
)
